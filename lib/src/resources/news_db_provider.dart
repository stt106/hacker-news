import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'dart:async';
import '../models/item_model.dart';
import 'respository.dart';

class NewsDbProvider implements Source, Cache {
  Database db;
  NewsDbProvider() {
    init();
  }
  init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, "items7.db");
    db = await openDatabase(path, version: 2,
        onCreate: (Database newDb, int version) {
      newDb.execute("""
          CREATE TABLE Items
          (
             id INTEGER PRIMARY KEY,
             type TEXT,
             by TEXT,
             title TEXT,
             time INTEGER,
             text TEXT,
             parent INTEGER,
             kids BLOB,
             dead INTEGER, 
             deleted INTEGER,
             descendants INTEGER,
             url TEXT,
             score INTEGER     
          )
          """);
    });
  }

  Future<ItemModel> fetchItem(int id) async {
    final maps = await db.query(
      'Items',
      columns: null,
      where: "id = ?",
      whereArgs: [id],
    );
    if (maps.length > 0) {
      return ItemModel.fromDb(maps.first);
    }
    return null;
  }

  Future<int> addItem(ItemModel item) {
    return db.insert('Items', item.toMap());
  }

  Future<List<int>> fetchTopIds() {
    //NullThrownError();
    throw Exception("doesn't support this");
    //return null;
  }

  Future<int> clear() {
    return db.delete('Items');
  }
}

final newsDbProvider = NewsDbProvider();
