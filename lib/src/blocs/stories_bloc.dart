import 'package:rxdart/rxdart.dart';
import '../models/item_model.dart';
import '../resources/respository.dart';
import 'dart:async';

class StoriesBloc {
  final _topIds = PublishSubject<List<int>>(); // streamcontroller
  final _repository = Respository();
  final _itemsOutput = BehaviorSubject<Map<int, Future<ItemModel>>>(); //single subscriber to itemsFetcher.
  final _itemsFetcher = PublishSubject<int>();

  
  StoriesBloc() {
    _itemsFetcher.stream.transform(_itemsTransformer()).pipe(_itemsOutput);
  }

  // expose the stream via a getter
  Observable<List<int>> get topIds => _topIds.stream;
  Observable<Map<int, Future<ItemModel>>> get items => _itemsOutput.stream;

  // Getters to sink
  Function(int) get fetchItem => _itemsFetcher.sink.add;



  // expose a function rather than a getter to avoid exposing sink directly.
  fetchTopIds() async {
    final ids = await _repository.fetchTopIds();
    _topIds.sink.add(ids);
  }

  clearCache() {
    return _repository.clearCache();
  }

  _itemsTransformer() {
    return ScanStreamTransformer(
      (Map<int, Future<ItemModel>> cache, int id, index) {
        //print(index);
        cache[id] = _repository.fetchItem(id);
        return cache;
      },
      <int, Future<ItemModel>>{},
    );
  }

  dispose() {
    _topIds.close();
    _itemsFetcher.close();
    _itemsOutput.close();
  }
}
