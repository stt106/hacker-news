import 'package:rxdart/rxdart.dart';
import '../models/item_model.dart';
import '../resources/respository.dart';
import 'dart:async';

class CommentsBloc {
  final _commentsFetcher = PublishSubject<int>();
  final _commentsOutput = BehaviorSubject<Map<int, Future<ItemModel>>>();
  final _repository = Respository();
  // Streams
  Observable<Map<int, Future<ItemModel>>> get itemWithComments =>
      _commentsOutput.stream;

  // Sink
  Function(int) get fetchItemWithComments => _commentsFetcher.sink.add;

  CommentsBloc() {
    _commentsFetcher.stream
        .transform(_commentsTransformer())
        .pipe(_commentsOutput);
  }

  void dispose() {
    _commentsFetcher.close();
    _commentsOutput.close();
  }

  // return a recursive transformer.
  _commentsTransformer() {
    return ScanStreamTransformer<int, Map<int, Future<ItemModel>>>(
      (cache, int id, index) {
        print('comment block $index'); // This is only called twice!
        cache[id] = _repository.fetchItem(id);
        cache[id].then((ItemModel item) {
          item.kids.forEach((kidId) =>
              fetchItemWithComments(kidId)); // rescurive data fetch!!!
        });
      },
      <int, Future<ItemModel>>{},
    );
  }
}
